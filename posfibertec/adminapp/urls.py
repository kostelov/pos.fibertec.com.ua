from django.urls import re_path
import adminapp.views as adminapp

app_name = 'adminapp'

urlpatterns = [
    re_path(r'^$', adminapp.main, name='main'),
    re_path(r'^login/$', adminapp.login, name='login'),
    re_path(r'^logout/$', adminapp.logout, name='logout'),
    re_path(r'^password/(?P<user_pk>\d+)/$', adminapp.changepass, name='changepass'),
    re_path(r'^header/$', adminapp.header, name='header'),
    re_path(r'^home/$', adminapp.home, name='home'),
    re_path(r'^slider/add/$', adminapp.slider_add, name='slider_add'),
    re_path(r'^slider/update/(?P<slider_pk>\d+)/$', adminapp.slider_update, name='slider_update'),
    re_path(r'^caption/(?P<caption_pk>\d+)/$', adminapp.caption, name='caption'),
    re_path(r'^content/$', adminapp.content, name='content'),
    re_path(r'^content/update/$', adminapp.content_update, name='content_update'),
    re_path(r'^choose/$', adminapp.choose, name='choose'),
]
