from django import forms
from django.contrib.auth.forms import AuthenticationForm, AdminPasswordChangeForm
from django.contrib.auth.models import User
from adminapp.models import Header, Slider, Caption, Content, Choose


class UserLoginForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ('username', 'password')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            if field_name == 'username':
                field.widget.attrs['placeholder'] = 'Имя пользователя'
            if field_name == 'password':
                # field.widget = forms.HiddenInput()
                field.widget.attrs['placeholder'] = 'Пароль'


class UserPassChangeForm(AdminPasswordChangeForm):
    class Meta:
        model = User
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            if field_name == 'password1':
                field.widget.attrs['placeholder'] = 'Пароль'
            if field_name == 'password2':
                field.widget.attrs['placeholder'] = 'Пароль (еще раз)'
            field.help_text = ''


class HeaderForm(forms.ModelForm):
    class Meta:
        model = Header()
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            if field_name == 'logo':
                field.widget.attrs['id'] = 'id-input-file-2'
            if field_name == 'alt':
                field.widget.attrs['placeholder'] = 'алтернативный текст'
            if field_name == 'about' or field_name == 'services' or field_name == 'portfolio' or field_name == 'contact':
                field.widget.attrs['placeholder'] = 'название меню'
            if field_name == 'about_active' or field_name == 'services_active' or field_name == 'portfolio_active' or field_name == 'contact_active':
                field.widget.attrs['class'] = 'ace ace-switch ace-switch-4'
            field.help_text = ''


class SliderForm(forms.ModelForm):
    class Meta:
        model = Slider
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            if field_name == 'title':
                field.widget.attrs['placeholder'] = 'Заголовок слайдера до 50 символов'
            if field_name == 'text':
                field.widget.attrs['placeholder'] = 'Краткое описание к слайдеру до 100 символов'
            if field_name == 'img':
                field.widget.attrs['id'] = 'id-input-file-2'
            if field_name == 'alt':
                field.widget.attrs['placeholder'] = 'алтернативный текст'
            if field_name == 'is_active':
                field.widget.attrs['class'] = 'ace ace-switch ace-switch-4'


class CaptionForm(forms.ModelForm):
    class Meta:
        model = Caption
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            if field_name == 'title':
                field.widget.attrs['placeholder'] = 'Заголовок до 100 символов'
            if field_name == 'text':
                field.widget.attrs['placeholder'] = 'Краткое описание до 500 символов'
            if field_name == 'is_active':
                field.widget.attrs['class'] = 'ace ace-switch ace-switch-4'


class ContentForm(forms.ModelForm):
    class Meta:
        model = Content
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            if field_name == 'title1' or field_name == 'title2' or field_name == 'title3' or field_name == 'title4':
                field.widget.attrs['placeholder'] = 'Заголовок до 50 символов'
            if field_name == 'text1' or field_name == 'text2' or field_name == 'text3' or field_name == 'text4':
                field.widget.attrs['placeholder'] = 'Длинна текста не более 300 символов'


class ChooseForm(forms.ModelForm):
    class Meta:
        model = Choose
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            if field_name == 'title':
                field.widget.attrs['placeholder'] = 'Заголовок до 50 символов'
            if field_name == 'text':
                field.widget.attrs['placeholder'] = 'Длинна текста не более 1000 символов'
