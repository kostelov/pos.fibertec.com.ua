from django.db import models


class Header(models.Model):
    logo = models.ImageField(verbose_name='логотип', upload_to='img', blank=True)
    alt = models.CharField(verbose_name='алтернативный текст', max_length=20, blank=True)
    about = models.CharField(verbose_name='о нас', max_length=20, blank=True)
    about_active = models.BooleanField(verbose_name='показать', default=True)
    services = models.CharField(verbose_name='услуги', max_length=20, blank=True)
    services_active = models.BooleanField(verbose_name='показать', default=True)
    portfolio = models.CharField(verbose_name='портфолио', max_length=20, blank=True)
    portfolio_active = models.BooleanField(verbose_name='показать', default=True)
    contact = models.CharField(verbose_name='контакты', max_length=20, blank=True)
    contact_active = models.BooleanField(verbose_name='показать', default=True)

    @staticmethod
    def header_create():
        header = Header(
            logo='img/logo.png',
            alt='логотип компании',
            about='',
            about_active=True,
            services='',
            services_active=True,
            portfolio='',
            portfolio_active=True,
            contact='',
            contact_active=True
        )
        return header


class Slider(models.Model):
    title = models.CharField(verbose_name='заголовок слайдера', max_length=50)
    text = models.CharField(verbose_name='краткое описание', max_length=100, blank=True)
    img = models.ImageField(verbose_name='слайдер', upload_to='slides', blank=True)
    alt = models.CharField(verbose_name='алтернативный текст', max_length=30, blank=True)
    is_active = models.BooleanField(verbose_name='показать', default=True)

    @staticmethod
    def slider_create():
        slider = Slider(
            title='',
            text='',
            img='img/1.jpg',
            alt='',
            is_active=True
        )
        return slider


class Caption(models.Model):
    title = models.CharField(verbose_name='приветственный заголовок', max_length=100, blank=True)
    text = models.TextField(verbose_name='краткое описание', max_length=500, blank=True)
    is_active = models.BooleanField(verbose_name='показать', default=True)

    @staticmethod
    def caption_create(request):
        if request.resolver_match.app_names:
            caption = Caption(
                title='',
                text='',
                is_active=True
            )
        else:
            caption = Caption(
                title='Welcome to our Company Website!',
                text='''Lorem ipsum dolor sit amet, unde omnis iste natus tote natus error sit voluptatem accusanti quas
                 potenti maltam rem aperiam, eaque ipsa quae ab illitecto beatae vitae dicemo enim ipsam voluptatem quia
                  voluptas sit aspernatur.''',
                is_active=True
            )
        return caption


class Content(models.Model):
    title1 = models.CharField(verbose_name='заголовок', max_length=50, blank=True)
    text1 = models.TextField(verbose_name='краткое описание', max_length=300, blank=True)
    title2 = models.CharField(verbose_name='заголовок', max_length=50, blank=True)
    text2 = models.TextField(verbose_name='краткое описание', max_length=300, blank=True)
    title3 = models.CharField(verbose_name='заголовок', max_length=50, blank=True)
    text3 = models.TextField(verbose_name='краткое описание', max_length=300, blank=True)
    title4 = models.CharField(verbose_name='заголовок', max_length=50, blank=True)
    text4 = models.TextField(verbose_name='краткое описание', max_length=300, blank=True)

    @staticmethod
    def content_create():
        content = Content(
            title1='',
            text1='',
            title2='',
            text2='',
            title3='',
            text3='',
            title4='',
            text4='',
        )
        return content

    @staticmethod
    def content_default():
        content = Content(
            title1='Legimusinm',
            text1='''Lorem ipsum dolor sit amet, unde omnis iste natus error sit voluptatem accusanti quas potenti 
            malesuada vel phasellus.''',
            title2='Serspiciatis',
            text2='''Lorem ipsum dolor sit amet, unde omnis iste natus error sit voluptatem accusanti quas potenti 
            malesuada vel phasellus.''',
            title3='Quisqugesta',
            text3='''Lorem ipsum dolor sit amet, unde omnis iste natus error sit voluptatem accusanti quas potenti 
            malesuada vel phasellus.''',
            title4='Architecto',
            text4='''Lorem ipsum dolor sit amet, unde omnis iste natus error sit voluptatem accusanti quas potenti 
            malesuada vel phasellus.''',
        )
        return content


class Choose(models.Model):
    title = models.CharField(verbose_name='заголовок', max_length=50, blank=True)
    text = models.TextField(verbose_name='описание', max_length=1000, blank=True)

    @staticmethod
    def choose_create(request):
        if request.resolver_match.app_names:
            choose = Choose(
                title='',
                text='',
            )
        else:
            choose = Choose(
                title='Why Choose Us?',
                text='''Sed ut perspiciaatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae 
                vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur. Sed ut 
                perspiciaatis iste natus error sit voluptatem probably haven't heard of them accusamus. Voluptatem 
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et 
                eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo quasi 
                architecto beatae vitae dicta sunt explicabo.''',
            )
        return choose
