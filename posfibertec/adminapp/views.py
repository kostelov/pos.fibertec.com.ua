from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, HttpResponseRedirect, get_object_or_404
from django.contrib import auth
from django.urls import reverse
from django.conf import settings
from django.contrib.auth.models import User
from adminapp.models import Header, Slider, Caption, Content, Choose
from adminapp.forms import UserLoginForm, UserPassChangeForm, HeaderForm, SliderForm, CaptionForm, ContentForm, \
    ChooseForm


@login_required()
def main(request):
    title = 'Админка'
    context = {
        'title': title,
    }

    return render(request, 'adminapp/main.html', context)


def login(request):
    title = 'Вход'
    form = UserLoginForm()
    next_page = request.GET['next'] if 'next' in request.GET.keys() else ''
    if request.method == 'POST':
        form = UserLoginForm(data=request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = auth.authenticate(request, username=username, password=password)
            if user and user.is_active:
                auth.login(request, user)
                if 'next' in request.POST.keys():
                    return HttpResponseRedirect(request.POST['next'])
                else:
                    return HttpResponseRedirect(reverse('admin:main'))

    context = {
        'title': title,
        'form': form,
        'next': next_page,
    }
    return render(request, 'adminapp/login.html', context)


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(settings.LOGIN_URL)


@login_required()
@user_passes_test(lambda user: user.is_superuser)
def changepass(request, user_pk):
    title = 'Сменить пароль'
    user = get_object_or_404(User, pk=user_pk)

    if user:
        if request.method == 'POST':
            form = UserPassChangeForm(user, data=request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('admin:main'))
        else:
            form = UserPassChangeForm(user)

        context = {
            'title': title,
            'form': form,
        }
        return render(request, 'adminapp/password.html', context)


@login_required()
@user_passes_test(lambda user: user.is_superuser)
def header(request):
    title = 'Шапка сайта'
    site_header = Header.objects.first() if Header.objects.first() else Header.header_create()
    if site_header:
        if request.method == 'POST':
            form = HeaderForm(request.POST, request.FILES, instance=site_header)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('admin:header'))
        else:
            form = HeaderForm(instance=site_header)

    active = ('active', '', '', '', '', '', '')
    context = {
        'title': title,
        'active': active,
        'form': form,
        'object': site_header,
    }
    return render(request, 'adminapp/header.html', context)


@login_required()
@user_passes_test(lambda user: user.is_superuser)
def home(request):
    title = 'Главная страница'
    slider = Slider.objects.all() if Slider.objects.all() else Slider.slider_create()

    active = ('', 'active', '', '', '', '', '')
    active_ul = ('active open')
    context = {
        'title': title,
        'active': active,
        'active_ul': active_ul,
        'slider_object': slider,
    }
    return render(request, 'adminapp/home.html', context)


@login_required()
@user_passes_test(lambda user: user.is_superuser)
def slider_add(request):
    title = 'Добавить слайдер'
    slider = Slider.slider_create()

    if request.method == 'POST':
        form = SliderForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('admin:home'))
    else:
        form = SliderForm()

    active = ('', 'active', '', '', '', '', '')
    active_ul = ('active open')
    context = {
        'title': title,
        'active': active,
        'active_ul': active_ul,
        'form': form,
        'object': slider,
    }
    return render(request, 'adminapp/slider_add.html', context)


@login_required()
@user_passes_test(lambda user: user.is_superuser)
def slider_update(request, slider_pk=None):
    title = 'Изменить слайдер'
    slider = get_object_or_404(Slider, pk=slider_pk)

    if slider:
        if request.method == 'POST':
            form = SliderForm(request.POST, request.FILES, instance=slider)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('admin:home'))
        else:
            form = SliderForm(instance=slider)
    else:
        # выполняем в том случае, если БД еще пустая
        slider = Slider.slider_create()
        form = SliderForm(instance=slider)

    active = ('', 'active', '', '', '', '', '')
    active_ul = ('active open')
    context = {
        'title': title,
        'active': active,
        'active_ul': active_ul,
        'form': form,
        'object': slider,
    }
    return render(request, 'adminapp/slider.html', context)


@login_required()
@user_passes_test(lambda user: user.is_superuser)
def caption(request, caption_pk=None):
    title = 'Заголовок'
    if int(caption_pk) != 0:
        caption = get_object_or_404(Caption, pk=caption_pk)
    else:
        caption = Caption.objects.first() if Caption.objects.first() else Caption.caption_create(request)

    print(caption.pk)
    if caption:
        if request.method == 'POST':
            form = CaptionForm(request.POST, instance=caption)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            form = CaptionForm(instance=caption)

    active = ('', 'active', '', '', '', '', '')
    active_ul = ('active open')
    context = {
        'title': title,
        'active': active,
        'active_ul': active_ul,
        'caption_object': caption,
        'form': form,
    }
    return render(request, 'adminapp/caption.html', context)


@login_required()
@user_passes_test(lambda user: user.is_superuser)
def content(request):
    title = 'Контент'
    content = Content.objects.first() if Content.objects.first() else Content.content_default()

    active = ('', 'active', '', '', '', '', '')
    active_ul = ('active open')
    context = {
        'title': title,
        'active': active,
        'active_ul': active_ul,
        'content_object': content,
    }
    return render(request, 'adminapp/content.html', context)


@login_required()
@user_passes_test(lambda user: user.is_superuser)
def content_update(request):
    title = 'Изменить контент'
    content = Content.objects.first() if Content.objects.first() else Content.content_create()

    if content:
        if request.method == 'POST':
            form = ContentForm(request.POST, instance=content)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('admin:content'))
        else:
            form = ContentForm(instance=content)

    active = ('', 'active', '', '', '', '', '')
    active_ul = ('active open')
    context = {
        'title': title,
        'active': active,
        'active_ul': active_ul,
        'form': form,
        'content_object': content,
    }
    return render(request, 'adminapp/content_update.html', context)


@login_required()
@user_passes_test(lambda user: user.is_superuser)
def choose(request):
    title = 'Почему мы?'
    choose = Choose.objects.first() if Choose.objects.first() else Choose.choose_create(request)
    if choose:
        if request.method == 'POST':
            form = ChooseForm(request.POST, instance=choose)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('admin:choose'))
        else:
            form = ChooseForm(instance=choose)

    active = ('', 'active', '', '', '', '', '')
    active_ul = ('active open')
    context = {
        'title': title,
        'active': active,
        'active_ul': active_ul,
        'form': form,
    }
    return render(request, 'adminapp/choose.html', context)
