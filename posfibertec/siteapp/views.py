from django.shortcuts import render
from adminapp.models import Header, Slider, Caption, Content, Choose


def index(request):
    title = 'FiberTec'
    site_header = Header.objects.first() if Header.objects.first() else Header.header_create()
    slider = Slider.objects.filter(is_active=True)
    caption = Caption.objects.first() if Caption.objects.first() else Caption.caption_create(request)
    content = Content.objects.first() if Content.objects.first() else Content.content_default()
    choose = Choose.objects.first() if Choose.objects.first() else Choose.choose_create(request)

    active = ('active', '', '', '', '')
    context = {
        'title': title,
        'active': active,
        'site_header': site_header,
        'slider': slider,
        'caption_object': caption,
        'content_object': content,
        'choose_object': choose,
    }

    return render(request, 'siteapp/index.html', context)


def about(request):
    site_header = Header.objects.first() if Header.objects.first() else Header.header_create()
    title = site_header.about
    active = ('', 'active', '', '', '')
    context = {
        'title': title,
        'active': active,
        'site_header': site_header,
    }

    return render(request, 'siteapp/about.html', context)


def services(request):
    site_header = site_header = Header.objects.first() if Header.objects.first() else Header.header_create()
    title = site_header.services
    active = ('', '', 'active', '', '')
    context = {
        'title': title,
        'active': active,
        'site_header': site_header,
    }

    return render(request, 'siteapp/services.html', context)


def portfolio(request):
    site_header = site_header = Header.objects.first() if Header.objects.first() else Header.header_create()
    title = site_header.portfolio
    active = ('', '', '', 'active', '')
    context = {
        'title': title,
        'active': active,
        'site_header': site_header,
    }

    return render(request, 'siteapp/portfolio.html', context)


def contact(request):
    site_header = site_header = Header.objects.first() if Header.objects.first() else Header.header_create()
    title = site_header.contact
    active = ('', '', '', '', 'active')
    context = {
        'title': title,
        'active': active,
        'site_header': site_header,
    }

    return render(request, 'siteapp/contact.html', context)
