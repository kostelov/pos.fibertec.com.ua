from django import template

register = template.Library()

URL_PREFIX = '/media/'


# @register.filter(name='default_image')
# @register.filter
# def default_image(string):
#     if not string:
#         string = 'products_image/no_image.png'
#
#     new_string = f'{URL_PREFIX}{string}'
#     return new_string


@register.filter
def default_img(string):
    if not string:
        string = 'img/logo.png'

    new_string = f'{URL_PREFIX}{string}'
    return new_string


@register.filter
def default_slider(string):
    if not string:
        string = 'img/1.jpg'

    new_string = f'{URL_PREFIX}{string}'
    return new_string
